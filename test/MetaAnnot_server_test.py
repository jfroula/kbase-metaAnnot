# -*- coding: utf-8 -*-
import unittest
import os  # noqa: F401
import json  # noqa: F401
import time
import requests
import shutil
import sys
import re
from os import environ
try:
    from ConfigParser import ConfigParser  # py2
except:
    from configparser import ConfigParser  # py3

from pprint import pprint  # noqa: F401

from biokbase.workspace.client import Workspace as workspaceService
from MetaAnnot.MetaAnnotImpl import MetaAnnot
from MetaAnnot.MetaAnnotServer import MethodContext
from MetaAnnot.authclient import KBaseAuth as _KBaseAuth

from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from DataFileUtil.DataFileUtilClient import DataFileUtil
from MetaAnnot.Utils.DataStagingUtils import DataStagingUtils
from SetAPI.SetAPIServiceClient import SetAPI
from GenomeFileUtil.GenomeFileUtilClient import GenomeFileUtil
from MetagenomeUtils.MetagenomeUtilsClient import MetagenomeUtils

class MetaAnnotTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        token = environ.get('KB_AUTH_TOKEN', None)
        config_file = environ.get('KB_DEPLOYMENT_CONFIG', None)
        cls.cfg = {}
        config = ConfigParser()
        config.read(config_file)
        for nameval in config.items('MetaAnnot'):
            cls.cfg[nameval[0]] = nameval[1]
        # Getting username from Auth profile for token
        authServiceUrl = cls.cfg['auth-service-url']
        auth_client = _KBaseAuth(authServiceUrl)
        user_id = auth_client.get_user(token)
        # WARNING: don't call any logging methods on the context object,
        # it'll result in a NoneType error
        cls.ctx = MethodContext(None)
        cls.ctx.update({'token': token,
                        'user_id': user_id,
                        'provenance': [
                            {'service': 'MetaAnnot',
                             'method': 'please_never_use_it_in_production',
                             'method_params': []
                             }],
                        'authenticated': 1})
        cls.wsURL = cls.cfg['workspace-url']
        cls.wsClient = workspaceService(cls.wsURL)
        cls.serviceImpl = MetaAnnot(cls.cfg)
        cls.scratch = cls.cfg['scratch']
        cls.callback_url = os.environ['SDK_CALLBACK_URL']
        suffix = int(time.time() * 1000)
        cls.wsName = "test_meta_annot_" + str(suffix)
        cls.ws_info = cls.wsClient.create_workspace({'workspace': cls.wsName})
        
        cls.dfu = DataFileUtil(os.environ['SDK_CALLBACK_URL'], token=token)
        cls.au = AssemblyUtil(os.environ['SDK_CALLBACK_URL'], token=token)
        cls.setAPI = SetAPI(url=cls.cfg['srv-wiz-url'], token=token)
        cls.gfu = GenomeFileUtil(os.environ['SDK_CALLBACK_URL'], service_ver='dev')
        cls.mu = MetagenomeUtils(os.environ['SDK_CALLBACK_URL'])
        
        cls.prepare_data()

    @classmethod
    def prepare_data(cls):
        """
        Lets put everything on workspace
        """
        [OBJID_I, NAME_I, TYPE_I, SAVE_DATE_I, VERSION_I, SAVED_BY_I, WSID_I,
         WORKSPACE_I, CHSUM_I, SIZE_I, META_I] = range(11)  # object_info tuple
        
        #
        # building Assembly
        #
        assembly_filename = 'assembly.fa'
        cls.assembly_filename_path = os.path.join(cls.scratch, assembly_filename)
        shutil.copy(os.path.join("data", assembly_filename), cls.assembly_filename_path)
        # from scratch upload to workspace
        assembly_params = {
            'file': {'path': cls.assembly_filename_path},
            'workspace_name': cls.ws_info[1],
            'assembly_name': 'MyAssembly'
        }
        cls.assembly_ref = cls.au.save_assembly_from_fasta(assembly_params)
        
        # assembly2.fa
        assembly_filename = 'assembly2.fa'
        cls.assembly_filename_path = os.path.join(cls.scratch, assembly_filename)
        shutil.copy(os.path.join("data", assembly_filename), cls.assembly_filename_path)
        # from scratch upload to workspace
        assembly_params = {
            'file': {'path': cls.assembly_filename_path},
            'workspace_name': cls.ws_info[1],
            'assembly_name': 'MyAssembly2'
        }
        cls.assembly_ref2 = cls.au.save_assembly_from_fasta(assembly_params)
        
        # create an AssemblySet
        assembly_items = [{'ref': cls.assembly_ref, 'label': 'assembly_1'},
                          {'ref': cls.assembly_ref2, 'label': 'assembly_2'}]
        assemblySet_obj = {'description': 'test assembly set',
                           'items': assembly_items}
        assemblySet_objname = 'TEST_ASSEMBLY_SET'
        cls.assemblySet_ref = cls.setAPI.save_assembly_set_v1({
                                        'workspace_name': cls.ws_info[1],
                                        'output_object_name': assemblySet_objname,
                                        'data': assemblySet_obj})['set_ref']
        
        #
        # create a BinnedContigs object
        #
        binned_contigs_dir_name = 'binned_contigs'
        binned_contigs_objname = 'MyBins_test3'
        binned_contigs_dir_path = os.path.join(cls.scratch, binned_contigs_dir_name)
        shutil.copytree(os.path.join("data", binned_contigs_dir_name), binned_contigs_dir_path)
        binned_contigs_params = {'file_directory': binned_contigs_dir_path,
                                 'workspace_name': cls.ws_info[1],
                                 'assembly_ref': cls.assembly_ref,
                                 'binned_contig_name': binned_contigs_objname
                                 }
        cls.binned_contigs_ref = cls.mu.file_to_binned_contigs(
                                                binned_contigs_params)['binned_contig_obj_ref']
        pprint('Saved BinnedContigs: ' + cls.binned_contigs_ref)
        
        #
        # upload a few genomes
        # 
        cls.genome_refs = []
        for i, genome_filename in enumerate(['bacteria.1.genomic.gbff',
                                            'bacteria.10.genomic.gbff']):
            genome_file_path = os.path.join(cls.scratch, genome_filename)
            shutil.copy(os.path.join("data", genome_filename), genome_file_path)
            cls.genome_refs.append(cls.gfu.genbank_to_genome({'file': {'path': genome_file_path},
                                                              'workspace_name': cls.ws_info[1],
                                                              'genome_name': genome_filename,
                                                              'generate_ids_if_needed': 1})['genome_ref'])
        
        #
        # create a genomeSet
        #
        genome_scinames = dict()
        for genome_i, genome_ref in enumerate(cls.genome_refs):
            genome_scinames[genome_ref] = 'Genus species str. '+str(genome_i)
        testGS = {
            'description': 'genomeSet for testing',
            'elements': dict()
        }
        for genome_ref in cls.genome_refs:
            testGS['elements'][genome_scinames[genome_ref]] = {'ref': genome_ref}
        obj_info = cls.wsClient.save_objects({'workspace': cls.ws_info[1],
                                              'objects': [
                                                  {
                                                      'type': 'KBaseSearch.GenomeSet',
                                                      'data': testGS,
                                                      'name': 'test_genomeset_1',
                                                      'meta': {},
                                                      'provenance': [
                                                          {
                                                              'service': 'kb_Msuite',
                                                              'method': 'test_CheckM'
                                                          }
                                                      ]
                                                  }]})[0]
        cls.genomeSet_ref = str(obj_info[WSID_I]) + '/' + str(obj_info[OBJID_I]) + '/' + str(obj_info[VERSION_I])
        
        
        
    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'wsName'):
            cls.wsClient.delete_workspace({'workspace': cls.wsName})
            print('Test workspace was deleted')

    def getWsClient(self):
        return self.__class__.wsClient

    def getWsName(self):
        return self.ws_info[1]

    def getImpl(self):
        return self.__class__.serviceImpl

    def getContext(self):
        return self.__class__.ctx

    # NOTE: According to Python unittest naming rules test method names should start from 'test'. # noqa
    def test_data_staging(self):
        # test stage assembly
        dsu = DataStagingUtils(self.cfg, self.getContext())
        
        # test we can get assembly from assembly object
        staged_input = dsu.stage_input(self.assembly_ref)
        self.assertTrue( re.search( 'assembly', ' '.join(staged_input), re.IGNORECASE), msg='Assembly could not be pulled from object: assembly\n{}'.format(staged_input))
        
        # test we can get assembly from assembly set object
        staged_input = dsu.stage_input(self.assemblySet_ref)
        self.assertTrue( re.search( 'assembly', ' '.join(staged_input), re.IGNORECASE ), msg='Assembly could not be pulled from object: assemblySet\n{}'.format(staged_input))
        
        # test we can get assembly from bins object
        staged_input = dsu.stage_input(self.binned_contigs_ref)
        self.assertTrue( re.search( 'assembly', ' '.join(staged_input), re.IGNORECASE ), msg='Assembly could not be pulled from object: binningObject\n{}'.format(staged_input))
        
        # test we can get assembly from genome object
        staged_input = dsu.stage_input(self.genome_refs)
        self.assertTrue( re.search( 'assembly', ' '.join(staged_input), re.IGNORECASE ), msg='Assembly could not be pulled from object: genome\n{}'.format(staged_input))
        
        # test we can get assembly from genome set object
        staged_input = dsu.stage_input(self.genomeSet_ref)
        self.assertTrue( re.search( 'assembly', ' '.join(staged_input), re.IGNORECASE ), msg='Assembly could not be pulled from object: genomeSet\n{}'.format(staged_input))
        
    def test_meta_annot_ok(self):

        # call your implementation
        ret = self.getImpl().annotate_contigs(self.getContext(),
                                            {'workspace_name': self.getWsName(),
                                             'assembly_or_genome_ref': self.assembly_ref,
                                             'min_length': 500
                                             })




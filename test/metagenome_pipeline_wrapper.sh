#!/bin/bash
set -x

usage()
{
cat<<EOF
  Usage: $0
    <-i contigs as fasta (required)>
    <-d directory with all pfam/rfam refdata (required)>
    <-o outfile (optional[annotation_results])>
    <-t threads [1] (optional)>

    defaults are shown in square brackets
EOF
exit 1
}


# check for all required args
ARGS=$*
if [[ $ARGS =~ "-i" ]] && [[ $ARGS =~ "-d" ]]; then
    echo;
else
    echo -e "Missing some required arguments\n"
    usage
fi


while getopts 'i:o:d:t:' OPTION
do
  case $OPTION in
  i)    CONTIGS="$OPTARG"
        ;;
  d)    DATA="$OPTARG"
        ;;
  o)    OUTDIR="$OPTARG"
        ;;
  t)    THREADS="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

OUTDIR=${OUTDIR:='annotation_results'}
THREADS=${THREADS:=1}

function testExecutable {
  exe=$1
  f=$(which $exe)
  if [[ $? > 0 ]]; then
    echo "Failed to find $exe.  Make sure its in your \$PATH"
    exit 1
  fi
}

testExecutable prodigal
testExecutable hmmsearch
testExecutable hmmer_tbl_gff
testExecutable cmsearch
testExecutable infernal_tbl_gff

if [[ ! $CONTIGS ]] || [[ ! $DATA ]]; then
	echo "no contigs: $CONTIGS or refdata: $DATA arguments were found"
  usage
fi

CONTIGS=$(readlink -f $CONTIGS)
DATA=$(readlink -f $DATA)

mkdir -p $OUTDIR && cd ${OUTDIR}
BASENAME=$(basename $CONTIGS)
if [[ ! -L $BASENAME ]]; then
  ln -s $CONTIGS .
fi
if [[ $? > 0 ]]; then
  echo "There was a problem trying to create a symlink of $CONTIGS to " $(pwd)
  exit 1
fi

#
# Prodigal
#
echo "running prodigal"
prodigal -c -n -f gff -p meta -q \
              -i $CONTIGS -o Prodigal.gff \
              -a Prodigal_proteins.faa -d Prodigal_genes.fna \
              -s Prodigal_starts.txt 

if [[ ! -s Prodigal_proteins.faa ]]; then
	echo "prodigal failed to produce a faa file"
	exit 1
fi

#
# Hmmsearch
#
#echo "running hmmsearch"
hmmsearch -o /dev/null --cpu $THREADS --domtblout Prodigal_Pfam.tbl --notextw --cut_tc $DATA/Pfam-A.hmm Prodigal_proteins.faa 
if [[ $? > 0 ]]; then
  echo "hmmsearch exited with error"
  exit 1
fi
hmmer_tbl_gff Prodigal_Pfam.tbl | filter_clans -c $DATA/Pfam_name_to_clan.tsv > Prodigal_Pfam.gff
if [[ ! -s Prodigal_Pfam.gff ]]; then
	echo "hmmer_tbl_gff failed to produce a gff file"
	exit 1
fi

#
# CMsearch
#
echo "running cmsearch"
cmsearch --cpu $THREADS -o /dev/null --tblout Rfam.tbl --notextw --cut_tc $DATA/Rfam.cm $CONTIGS 
if [[ $? > 0 ]]; then
  echo "cmsearch exited with error"
  exit 1
fi
infernal_tbl_gff < Rfam.tbl | filter_clans -c $DATA/Rfam_name_to_clan.tsv > Rfam.gff
if [[ ! -s Rfam.gff ]]; then
	echo "infernal_tbl_gff failed to produce a gff file"
	exit 1
fi
echo $0 Finished annotation

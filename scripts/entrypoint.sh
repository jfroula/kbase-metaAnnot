#!/bin/bash

. /kb/deployment/user-env.sh

python ./scripts/prepare_deploy_cfg.py ./deploy.cfg ./work/config.properties

if [ -f ./work/token ] ; then
  export KB_AUTH_TOKEN=$(<./work/token)
fi

if [ $# -eq 0 ] ; then
  sh ./scripts/start_server.sh
elif [ "${1}" = "test" ] ; then
  echo "Run Tests"
  make test
elif [ "${1}" = "async" ] ; then
  sh ./scripts/run_async.sh
elif [ "${1}" = "init" ] ; then
  echo "Initialize module"
  mkdir -p /data
  cd /data

  # download Pfams and Rfams
  wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz && gunzip Pfam-A.hmm.gz
  wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.cm.gz && gunzip Rfam.cm.gz
  
  # download clan to pfam or rfam tables and format them
  wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan.txt.gz --output-document=pfam_clan.txt.gz && gunzip pfam_clan.txt.gz
  awk '{print $2"\t"$1}' pfam_clan.txt > Pfam_name_to_clan.tsv
  rm pfam_clan.txt
  
  wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/database_files/clan.txt.gz --output-document=rfam_clan.txt.gz && gunzip rfam_clan.txt.gz
  awk '{print $2"\t"$1}' rfam_clan.txt > Rfam_name_to_clan.tsv
  rm rfam_clan.txt

  echo we are here $(pwd)
  echo and can see $(ls)
  if [ -s "/data/Rfam_name_to_clan.tsv" ] && \
     [ -s "/data/Pfam_name_to_clan.tsv" ] && \
     [ -s "/data/Pfam-A.hmm" ] && \
     [ -s "/data/Rfam.cm" ]; then
        touch /data/__READY__
  else
    echo "Init failed"
  fi
elif [ "${1}" = "bash" ] ; then
  bash
elif [ "${1}" = "report" ] ; then
  export KB_SDK_COMPILE_REPORT_FILE=./work/compile_report.json
  make compile
else
  echo Unknown
fi

# -*- coding: utf-8 -*-
#BEGIN_HEADER
# The header block is where all import statments should live
import os,sys
from Bio import SeqIO
from pprint import pprint, pformat
from MetaAnnot.Utils.MetaAnnotUtil import meta_annot
#END_HEADER


class MetaAnnot:
    '''
    Module Name:
    MetaAnnot

    Module Description:
    A KBase module: MetaAnnot
    '''

    ######## WARNING FOR GEVENT USERS ####### noqa
    # Since asynchronous IO can lead to methods - even the same method -
    # interrupting each other, you must be *very* careful when using global
    # state. A method could easily clobber the state set by another while
    # the latter method is running.
    ######################################### noqa
    VERSION = "1.5.3"
    GIT_URL = "git@gitlab.com:jfroula/kbase-metaAnnot.git"
    GIT_COMMIT_HASH = ""

    #BEGIN_CLASS_HEADER
    # Class variables and functions can be defined in this block
    #END_CLASS_HEADER

    # config contains contents of config file in a hash or None if it couldn't
    # be found
    def __init__(self, config):
        #BEGIN_CONSTRUCTOR
        # Any configuration parameters that are important should be parsed and
        # saved in the constructor.
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        self.shared_folder = config['scratch']
        self.config = config
        #END_CONSTRUCTOR
        pass


    def annotate_contigs(self, ctx, params):
        """
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
        :param params: instance of type "MetaAnnotParams" (A 'typedef' can
           also be used to define compound or container objects, like lists,
           maps, and structures.  The standard KBase convention is to use
           structures, as shown here, to define the input and output of your
           function.  Here the input is a reference to the Assembly data
           object, a workspace to save output, and a length threshold for
           filtering. To define lists and maps, use a syntax similar to C++
           templates to indicate the type contained in the list or map.  For
           example: list <string> list_of_strings; mapping <string, int>
           map_of_ints;) -> structure: parameter "assembly_or_genome_ref" of
           type "obj_ref" (An X/Y/Z style reference), parameter
           "workspace_name" of String, parameter "min_length" of Long
        :returns: instance of type "MetaAnnotResults" (Here is the definition
           of the output of the function.  The output can be used by other
           SDK modules which call your code, or the output visualizations in
           the Narrative.  'report_name' and 'report_ref' are special output
           fields- if defined, the Narrative can automatically render your
           Report.) -> structure: parameter "report_name" of String,
           parameter "report_ref" of String
        """
        # ctx is the context object
        # return variables are: output
        #BEGIN annotate_contigs
        # Print statements to stdout/stderr are captured and available as the App log
        print('Starting contig annotation function. Params=')
        pprint(params)

        for key, value in params.iteritems():
            if isinstance(value, basestring):
                params[key] = value.strip()

        annot_runner = meta_annot(self.config, ctx)
        output = annot_runner.run_meta_annot(params)
 
        #END annotate_contigs

        # At some point might do deeper type checking...
        if not isinstance(output, dict):
            raise ValueError('Method annotate_contigs return value ' +
                             'output is not type dict as required.')
        # return the results
        return [output]
    def status(self, ctx):
        #BEGIN_STATUS
        returnVal = {'state': "OK",
                     'message': "",
                     'version': self.VERSION,
                     'git_url': self.GIT_URL,
                     'git_commit_hash': self.GIT_COMMIT_HASH}
        #END_STATUS
        return [returnVal]

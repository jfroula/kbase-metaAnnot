import os,sys
import time
import glob
import subprocess
from pprint import pprint
from ConfigParser import ConfigParser
from Workspace.WorkspaceClient import Workspace
from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from SetAPI.SetAPIServiceClient import SetAPI
from MetagenomeUtils.MetagenomeUtilsClient import MetagenomeUtils
from DataFileUtil.DataFileUtilClient import DataFileUtil
from GenomeFileUtil.GenomeFileUtilClient import GenomeFileUtil

class DataStagingUtils(object):
    
    def verifyFastaisGood(self, fasta_path, min_fasta_len=1):
        '''
        counts the number of non-header, non-whitespace characters in a FASTA file
        '''
        seq_len = 0        
        # does it exist?
        if not os.path.isfile(fasta_path):
            raise ValueError('Error: fasta file not found {}'.format(fasta_path))
        
        # is it emtpy?
        with open (fasta_path, 'r', 0) as fasta_handle:
            for line in fasta_handle:
                line = line.strip()
                if line.startswith('>'):
                    continue
                line = line.replace(' ','')
                seq_len += len(line)
                if seq_len < min_fasta_len:
                    raise ValueError('File is empty: {}'.format(fasta_path))
                
    def __init__(self, config, ctx):
        self.ctx = ctx
        self.scratch = os.path.abspath(config['scratch'])
        self.ws_url = config['workspace-url']
        self.serviceWizardURL = config['srv-wiz-url']
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        token = os.environ.get('KB_AUTH_TOKEN', None)
        if not os.path.exists(self.scratch):
            os.makedirs(self.scratch)
        
        config_file = os.environ.get('KB_DEPLOYMENT_CONFIG', None)
        self.cfg = {}
        config = ConfigParser()
        config.read(config_file)
        for nameval in config.items('MetaAnnot'):
            self.cfg[nameval[0]] = nameval[1]
            
        self.wss = Workspace(self.cfg['workspace-url'])
        self.wsClient = Workspace(self.ws_url)
        suffix = int(time.time() * 1000)
        wsName = "kb_metaAnnot" + str(suffix)
        self.ws_info = self.wsClient.create_workspace({'workspace': wsName})
        self.ws_name = self.ws_info[1]
        
        self.dfu = DataFileUtil(os.environ['SDK_CALLBACK_URL'], token=token)

    def stage_input(self, input_ref):
        '''
        Stage input based on an input data reference for CheckM
        input_ref can be a reference to an Assembly, BinnedContigs or a Genome.
        This method creates a directory in the scratch area with the set of Fasta files, names
        will have the "fasta" file extension parameter tacked on.
            ex:
            staged_input = stage_input('124/15/1')
            staged_input
            {"input_dir": '...'}
        '''
        # config
        SERVICE_VER = 'dev'
        REQUIRED_MIN_FASTA_SIZE = 1
        # SERVICE_VER = 'release'

        # generate a folder in scratch to hold the input
        suffix = str(int(time.time() * 1000))
        input_dir = os.path.join(self.scratch, 'assemblies_' + suffix)
        all_seq_fasta = os.path.join(self.scratch, 'all_sequences_' + suffix + '.fasta')
        if not os.path.exists(input_dir):
            os.makedirs(input_dir)
        
        if isinstance(input_ref, list):
            # genome objects
            obj_info = self.dfu.get_objects( {'object_refs': input_ref} )
        else:
            # assembly and bin objects
            obj_info = self.dfu.get_objects( {'object_refs': [input_ref]} )
        type_name = obj_info['data'][0]['info'][2]
    
        # auClient
        try:
            auClient = AssemblyUtil(self.callback_url, token=self.ctx['token'], service_ver=SERVICE_VER)
        except Exception as e:
            raise ValueError('Unable to instantiate auClient with callback_url: '+ self.callback_url +' ERROR: ' + str(e))

        # setAPI_Client
        try:
            setAPI_Client = SetAPI (url=self.serviceWizardURL, token=self.ctx['token'])  # for dynamic service
        except Exception as e:
            raise ValueError('Unable to instantiate setAPI_Client with serviceWizardURL: '+ self.serviceWizardURL +' ERROR: ' + str(e))

        # mguClient
        try:
            mguClient = MetagenomeUtils(self.callback_url, token=self.ctx['token'], service_ver=SERVICE_VER)
        except Exception as e:
            raise ValueError('Unable to instantiate mguClient with callback_url: '+ self.callback_url +' ERROR: ' + str(e))

        # gfuClient
        try:
            gfuClient = GenomeFileUtil(self.callback_url, token=self.ctx['token'], service_ver=SERVICE_VER)
        except Exception as e:
            raise ValueError('Unable to instantiate gfuClient with callback_url: '+ self.callback_url +' ERROR: ' + str(e))
        
        
        # Standard Single Assembly
        if type_name.startswith('KBaseGenomeAnnotations.Assembly') or type_name.startswith('KBaseGenomes.ContigSet'):
            # create file data
            assembly_scratch_path = auClient.get_assembly_as_fasta({'ref': input_ref}).get('path')
            self.verifyFastaisGood(assembly_scratch_path,REQUIRED_MIN_FASTA_SIZE)
            
            print('found assembly from KBaseGenomeAnnotations.Assembly or KBaseGenomes.ContigSet')
            return [assembly_scratch_path]
        
        # AssemblySet
        #
        elif type_name.startswith('KBaseSets.AssemblySet'):
            # read assemblySet
            try:
                assemblySet_obj = setAPI_Client.get_assembly_set_v1 ({'ref':input_ref})
            except Exception as e:
                raise ValueError('Unable to get object from workspace: (' + input_ref +')' + str(e))
            assembly_set_paths = []
            
            for assembly_item in assemblySet_obj['data']['items']:            
                this_assembly_ref = assembly_item['ref']
                # assembly obj info
                try:
                    this_assembly_info = self.wss.get_object_info_new ({'objects':[{'ref':this_assembly_ref}]})[0]
                except Exception as e:
                    raise ValueError('Unable to get object from workspace: (' + this_assembly_ref +'): ' + str(e))
                
                assembly_path = auClient.get_assembly_as_fasta({'ref': this_assembly_ref})['path']
                
                self.verifyFastaisGood(assembly_path,REQUIRED_MIN_FASTA_SIZE)
                assembly_set_paths.append(assembly_path)
            print('found assembly from KBaseSets.AssemblySet')
            return assembly_set_paths
        
        #   
        # Binned Contigs
        #
        elif type_name.startswith('KBaseMetagenomes.BinnedContigs'):
            assembly_set_paths = []

            # download the bins as fasta and set the input folder name
            params = {
                'binned_contig_obj_ref': input_ref,
                'assembly_suffix': '_assembly',
                'assembly_set_name': 'assembly_set',
                'extracted_assemblies': '',
                'workspace_name': self.ws_name
            }
            
            assembly_ref_list = mguClient.extract_binned_contigs_as_assembly(params)['assembly_ref_list']
        
            for myref in assembly_ref_list:
                assembly_scratch_path = auClient.get_assembly_as_fasta({'ref': myref}).get('path')
                assembly_set_paths.append(assembly_scratch_path)
                
                self.verifyFastaisGood(assembly_scratch_path,REQUIRED_MIN_FASTA_SIZE)
            print('KBaseMetagenomes.BinnedContigs')        
            return assembly_set_paths
        
        # Genome and GenomeSet
        #
        elif type_name.startswith('KBaseGenomes.Genome'):
            assembly_ref_list = obj_info['data'][0]['data']['assembly_ref']
            assembly_scratch_path = auClient.get_assembly_as_fasta({'ref': assembly_ref_list}).get('path')
            print('found assembly from KBaseGenomes.Genome')
            return [assembly_scratch_path]
        
        elif type_name.startswith('KBaseSearch.GenomeSet'):
            assembly_paths = []
            assembly_ref_dict = obj_info['data'][0]['data']['elements']
            for species in assembly_ref_dict:
                genome_ref = assembly_ref_dict[species]['ref']
                genome_obj_info = self.dfu.get_objects( {'object_refs': [genome_ref]} )

                assembly_ref = genome_obj_info['data'][0]['info'][10]['Assembly Object'] # ['assembly_ref']
                assembly_scratch_path = auClient.get_assembly_as_fasta({'ref': assembly_ref})['path']
                assembly_paths.append(assembly_scratch_path)

                self.verifyFastaisGood(assembly_scratch_path,REQUIRED_MIN_FASTA_SIZE)
            print('found assembly from KBaseSearch.GenomeSet')
            return assembly_paths
        
        # Unknown type slipped through
        #
        else:
            raise ValueError('Cannot stage fasta file input directory from type: ' + type_name)
                    

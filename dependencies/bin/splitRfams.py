#!/usr/bin/env python
# Summary:
# --------
# This script will divide the Rfam.cm reference datareference file into 10
# subfiles so cmsearch can run in parallel (8 jobs x 4 cpu each).  This seems to
# speed up the cmsearch compared to running it it with --cpu=16 on the full Rfam.cm file.
#
# Assumptions:
# ------------
# We're assuming there are 32 cpus on the running node since
# I'm running cmsearch in parallel (jobs=8 with --cpu=4)
#

import sys,os
import re
import math

if len(sys.argv) < 3:
	print ("Usage: " + sys.argv[0] + " <Rfam.cm file> <output split dir>")
	sys.exit(1)

file=sys.argv[1]
splitdir=sys.argv[2]

os.makedirs(splitdir)

if not os.path.exists(file):
	print ("file doesn't exist: {}".format(file))
	sys.exit(1)

begin=0
subfiles=10.0 # this needs to be a float

### Main ###
f=open(file,'r')
file_str = f.read()
f.close()

# parse Rfam.cm into individual profiles
file_list = re.split('INFERNAL',file_str)
num_profiles = len(file_list)
print ("### ----------------------- ###")
print ("num_profiles: {}".format(num_profiles))

# break the file up into X sub-files
# and round up int so we catch all CM profiles
profiles_in_each_file = num_profiles/subfiles
profiles_in_each_file = int(math.ceil(profiles_in_each_file))
print ("profiles_in_each_file: {}".format(profiles_in_each_file))
print ("there are {} subfiles files".format(subfiles))
print ("### ----------------------- ###")


for i in range(int(subfiles)):
	out_file = splitdir + "/Rfam.cm." + str(i)
	out=open(out_file,'w')
	end   = begin + profiles_in_each_file
	if i == 0:
		file_content = ''
	else:
		file_content = 'INFERNAL'

	file_content += 'INFERNAL'.join(file_list[begin:end])
	print ("writing to file: {}".format(out_file))
	out.write(file_content)
	out.close()
	begin = end 

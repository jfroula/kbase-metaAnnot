#!/usr/bin/env python
# Summary:
# --------
# This script will divide the Rfam.cm reference datareference file into 10
# subfiles so cmsearch can run in parallel (8 jobs x 4 cpu each).  This seems to
# speed up the cmsearch compared to running it it with --cpu=16 on the full Rfam.cm file.
#
# Assumptions:
# ------------
# We're assuming there are 32 cpus on the running node since
# I'm running cmsearch in parallel (jobs=8 with --cpu=4)
#

import sys,os
import re
import math
from pprint import pprint
from Bio import SeqIO

if len(sys.argv) < 4:
	print ("Usage: " + sys.argv[0] + " <num_split_files> <assembly.fa> <output split dir>")
	sys.exit(1)

num_split_files=int(sys.argv[1])
contigs=sys.argv[2]
splitdir=sys.argv[3]

def _mkdir_p(path):
        """
        _mkdir_p: make directory for given path
        """
        if not path:
            return
        try:
            os.makedirs(path)
        except OSError as exc:
            if os.path.isdir(path):
                pass
            else:
                raise

def writeSplitFile(myOutFiles,counter):
	SeqIO.write(record, myOutFiles[counter-1], "fasta")
		
		
_mkdir_p(splitdir)

if not os.path.exists(contigs):
	print ("file doesn't exist: {}".format(file))
	sys.exit(1)

begin=0
subfiles=10.0 # this needs to be a float

### Main ###
# create a list of 4 file handles to print to
myOutFiles = [ open(splitdir+'/'+'split'+str(i)+'.fa', 'wb') for i in range(num_split_files) ]

# split files into 4 files by taking every nth record since
# assembly file may be sorted by contig size and we want all
# split files to be roughly equal size.
counter=0			 
for i, record in enumerate(SeqIO.parse(contigs, "fasta")):
	i += 1  # lets start at index of 1 and not 0
	counter += 1
	
	writeSplitFile(myOutFiles,counter)
	
	if (i % num_split_files == 0):
		# reset counter
		counter=0
			
for f in myOutFiles:
	f.close()

	
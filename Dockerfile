FROM kbase/sdkbase2:latest
MAINTAINER KBase Developer
# -----------------------------------------
# In this section, you can install any system dependencies required
# to run your App.  For instance, you could place an apt-get update or
# install line here, a git checkout to download code, or run any other
# installation scripts.

COPY test/metagenome_pipeline_wrapper.sh /kb/deployment/bin/metagenome_pipeline_wrapper.sh
ENV PATH=/kb/module/dependencies/bin:$PATH
# -----------------------------------------
RUN pip install biopython

COPY ./ /kb/module
RUN mkdir -p /kb/module/work
RUN chmod -R a+rw /kb/module

WORKDIR /kb/module

RUN make all

ENTRYPOINT [ "./scripts/entrypoint.sh" ]

CMD [ ]
